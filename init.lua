-- stupid nvim init.lua
-- I'm too stupid for complex configs

-- goals:
-- nvim exclusive
-- lua only
-- one file
-- enable cool new features
-- fewest plugins
-- mostly default
-- mostly async
-- make professional work painless
-- make vim feel pro
-- no rice

-- todo:
-- version control
-- migrate package manager to packer

-- interface
local opt = vim.opt
opt.mouse = 'a'
opt.number = true
opt.swapfile = false
opt.showmatch = true
-- opt.colorcolumn = '80'

-- formatting
opt.expandtab = true
opt.shiftwidth = 4
opt.tabstop = 4
opt.smartindent = true
-- FIXME: is this heuristic formatting?

-- setup Plug
vim.call('plug#begin', '~/.config/nvim/plugged')

local Plug = vim.fn['plug#']
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'jeffkreeftmeijer/vim-dim'
Plug 'noahfrederick/vim-noctu'
Plug('nvim-treesitter/nvim-treesitter', {
    ['do'] = function()
        vim.cmd('TSUpdate')
    end
})
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'mhartington/formatter.nvim'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'kyazdani42/nvim-tree.lua'
Plug 'lewis6991/gitsigns.nvim'

vim.call('plug#end')
vim.g.plug_window = 'new'

-- set some term colors
local cmd = vim.cmd
cmd('colorscheme dim')
-- cmd('colorscheme noctu')

-- setup nvim-cmp
local cmp = require('cmp')

cmp.setup {
    mapping={
        ['<C-b>']=cmp.mapping(cmp.mapping.scroll_docs(-4), {'i', 'c'}),
        ['<C-f>']=cmp.mapping(cmp.mapping.scroll_docs(4), {'i', 'c'}),
        ['<Tab>']=cmp.mapping(cmp.mapping.select_next_item(), {'i', 'c'}),
        ['<C-Space>']=cmp.mapping(cmp.mapping.complete(), {'i', 'c'}),
        ['<CR>']=cmp.mapping.confirm({select = true}),
    },
    sources={
        {name='nvim_lua'},
        {name='nvim_lsp'},
        {name='path'},
        -- {name = 'luasnip'},
        {name='buffer', keyword_length=3},
    },
}

cmp.setup.cmdline('/', {
    sources={
        {name='buffer'},
    },
})

cmp.setup.cmdline(':', {
    sources=cmp.config.sources({
        {name='path'},
    }, {
        {name='cmdline'}
    }),
})

local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())

-- set up lsp
local nvim_lsp = require('lspconfig')

local setup_bindings = function(_, bufnr)
    local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end
    buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

    local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
    local opts = {noremap=true, silent=true}
    buf_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
    buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
    buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
    buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
    buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
    buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
    buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
    buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
    buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
    buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
    buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
    buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
    buf_set_keymap('n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
    buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
    buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
    buf_set_keymap('n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
    buf_set_keymap('n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
end

local servers = {'rls', 'pyright'}
for _, lsp in ipairs(servers) do
    nvim_lsp[lsp].setup{
        on_attach=setup_bindings,
        flags={debounce_text_changes=150},
        capabilities=capabilities,
    }
end

-- hide undefined global vim
nvim_lsp['sumneko_lua'].setup{
    on_attach=setup_bindings,
    flags={debounce_text_changes=150},
    capabilities=capabilities,
    settings={
        Lua={
            diagnostics={
                globals={'vim'},
            }
        }
    }
}

-- setup telescope
require('telescope').setup{}

local function nvim_set_keymap(...) vim.api.nvim_set_keymap(...) end

local opts = {noremap=true, nowait=true}
nvim_set_keymap('n', 'ff', ('<Cmd>Telescope find_files<CR>'), opts)
nvim_set_keymap('n', 'fg', ('<Cmd>Telescope live_grep<CR>'), opts)
nvim_set_keymap('n', 'fb', ('<Cmd>Telescope buffers<CR>'), opts)
nvim_set_keymap('n', 'fh', ('<Cmd>Telescope help_tags<CR>'), opts)

-- setup treesitter
require('nvim-treesitter.configs').setup{
    ensure_installed = 'maintained',
    highlight={
        enable=true,
        additional_vim_regex_highlighting=false,
    },
    indent={
        enable=true,
    }
}

-- setup formatter
require('formatter').setup({
    filetype={
        python={
            function()
                return {
                    exe="black",
                    args={'-'},
                    stdin=true,
                }
            end
        }
    }
})

vim.api.nvim_exec([[
augroup FormatAutogroup
    autocmd!
    autocmd BufWritePost *.py FormatWrite
augroup END
]], true)

-- setup nvim-tree
require'nvim-tree'.setup {
  disable_netrw        = true,
  hijack_netrw         = true,
  open_on_setup        = true,
  ignore_ft_on_setup   = {},
  auto_close           = false,
  auto_reload_on_write = true,
  open_on_tab          = false,
  hijack_cursor        = false,
  update_cwd           = false,
  update_to_buf_dir    = {
    enable = true,
    auto_open = true,
  },
  diagnostics = {
    enable = false,
    icons = {
      hint = "",
      info = "",
      warning = "",
      error = "",
    }
  },
  update_focused_file = {
    enable      = false,
    update_cwd  = false,
    ignore_list = {}
  },
  system_open = {
    cmd  = nil,
    args = {}
  },
  filters = {
    dotfiles = false,
    custom = {}
  },
  git = {
    enable = true,
    ignore = true,
    timeout = 500,
  },
  view = {
    width = 30,
    height = 30,
    hide_root_folder = false,
    side = 'left',
    auto_resize = false,
    mappings = {
      custom_only = false,
      list = {}
    },
    number = false,
    relativenumber = false,
    signcolumn = "yes"
  },
  trash = {
    cmd = "trash",
    require_confirm = true
  },
  actions = {
    change_dir = {
      global = false,
    },
    open_file = {
      quit_on_open = false,
    }
  }
}

-- setup gitsigns defaults
require('gitsigns').setup()
